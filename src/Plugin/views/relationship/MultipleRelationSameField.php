<?php
namespace Drupal\multiplerelationsamefield\Plugin\views\relationship;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\relationship\RelationshipPluginBase;

/**
 * Default implementation of the base relationship plugin.
 *
 * @ingroup views_relationship_handlers
 *
 * @ViewsRelationship("multiplerelationsamefield")
 */
class MultipleRelationSameField extends RelationshipPluginBase {

    /**
     * {@inheritdoc}
     */
    protected function defineOptions() {
        $options = parent::defineOptions();

        $options['multiplerelationsamefield'] = ['default' => FALSE];

        return $options;
    }


    /**
     * {@inheritdoc}
     */
    public function ensureMyTable() {
        if (!empty($this->options['multiplerelationsamefield'])) {
            if (! isset($this->relationship)) {
                $this->relationship = $this->query->view->storage->get('base_table');
            }
            $base_table = $this->query->relationships[$this->relationship]['base'];
            $join = $this->query->getJoinData($this->table, $base_table);
            $alias = $this->definition['base'] . '_' . $this->table . '_bm';
            $this->tableAlias = $this->query->addRelationship($alias, $join, $this->definition['base'], $this->relationship);
        } else {
            $this->tableAlias = parent::ensureMyTable();
        }

        return $this->tableAlias;
    }


    public function buildOptionsForm(&$form, FormStateInterface $form_state) {
        parent::buildOptionsForm($form, $form_state);

        $form['multiplerelationsamefield'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Multiple relationships on same field'),
            '#description' => $this->t('Multiple relationships on same field'),
            '#default_value' => !empty($this->options['multiplerelationsamefield']),
        ];
    }

}
